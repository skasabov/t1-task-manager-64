package ru.t1.skasabov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.repository.ProjectRepository;

@Controller
public final class ProjectController {

    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @GetMapping("/project/create")
    public String create() {
        projectRepository.create();
        return "redirect:/projects";
    }

    @NotNull
    @GetMapping("/project/delete/{id}")
    public String delete(@PathVariable("id") @NotNull final String id) {
        projectRepository.removeById(id);
        return "redirect:/projects";
    }

    @NotNull
    @PostMapping("/project/edit/{project.id}")
    public String edit(@ModelAttribute("project") @NotNull final Project project) {
        projectRepository.save(project);
        return "redirect:/projects";
    }

    @NotNull
    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@PathVariable("id") @NotNull final String id) {
        @NotNull final Project project = projectRepository.findById(id);
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    @NotNull
    private Status[] getStatuses() {
        return Status.values();
    }

}
